# XMPP Providers - Curated List of Providers for Registration and Autocomplete

This project's goal is to provide a machine-readable curated list of [XMPP](https://xmpp.org/about/technology-overview.html) providers.
That JSON list can be used by XMPP clients for provider suggestions.

Each **client** has different requirements on a provider: The script in this repository can be used to create a **statically filtered** provider list.

Each **user** has different requirements on a provider: At runtime, the statically filtered list can be **dynamically filtered** on the user's demands.

## Categories

The script to create a filtered list can be used for specific requirements.
However, to provide a standardized way of categorizing providers, there are three main options for filtering the list.

The following categories are used to distinguish between different quality levels the checked provders can offer.
Category A offers the best, B an average and C the worst user experience during onboarding and further usage.

The category A is a subset of B which is in turn a subset of C.
Thus, providers of A can be used for the purposes of B and C as well.
Providers of B can be used for the purposes of C as well.
The unfiltered provider list corresponds to category C.

### Category A: Automatically Chosen

Providers in this category can be used for an automatic registration.

### Category B: Manually Selectable

Providers in this category can be used for a manual registration.

### Category C: Usable for Autocomplete

Providers in this category can be used for autocomplete when the user enters a chat address (JID).

## Usage Example

A filtered provider list is primarily used for registering an account with the XMPP client [Kaidan](https://invent.kde.org/network/kaidan).
The whole provider list is also going to be used to suggest and autocomplete the chat address when entering a chat address in Kaidan (e.g., while adding a contact).

Here are steps used by Kaidan, other clients might process the list similarly:
1. This project's provider list is filtered to create a list of providers which support registration (category A or B) (a) and another one which includes all providers (category C) (b).
1. The created lists are included into Kaidan's builds.
1. All providers in (a) which are good enough for belonging to category A are filtered at runtime and used to create an account automatically with [Kaidan's quick onboarding](https://www.kaidan.im/2020/01/08/Easy-Registration/).
1. All providers in (a) which are not good enough for belonging to category A but B are filtered at runtime and used to be displayed for Kaidan's manual registration.
1. All providers of (b) are used at runtime for chat address autocomplete.

Note:
Providers can support registration via [XEP-0077: In-Band Registration](https://xmpp.org/extensions/xep-0077.html) (preferred way) or via web registration (on a web page).
Therefore, if the provider does not support in-band registration, it must at least support web registration to be included in Kaidan's registration provider list.

## Properties

The properties of a provider are mapped to its server address (JID, e.g., example.org).

## Basic Information

The following properties do **not affect** the provider's **category**:

Information (Key in JSON File) | Description / Data Type / Unit of Measurement
---|---
lastCheck | [ISO YYYY-MM-DD date](https://en.wikipedia.org/wiki/ISO_8601#Dates) (e.g., 2021-01-16)
website | mappings from lower-case [two-letter (639-1) language code](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes) to link of website language version or {} for n/a
company | true or false
chatLanguages | list of lower-case [two-letter (639-1) language codes](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes) or [] for n/a
emailLanguages | list of lower-case [two-letter (639-1) language codes](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes) or [] for n/a
passwordResetWebPage | mappings from lower-case [two-letter (639-1) language code](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes) to link of web page language version or {} for n/a

## Criteria

The table shows the following two circumstances for category A and B:

1. **Can** the provider be in category X if the criterion Y does **not** apply?
The answer can be *y* for yes, *n* for no, or yes if a specific case like greater or lower than a specific value is applicable.
1. A provider **is** in category X, if all questions from 1. for that category have a positive answer.
That means, the provider is in a specific category when all statements in its column are applicable.

Here is an example for the in-band and web registration:
*If inBandRegistration is not supported, the provider is not in category A but it is in B if additionally a registrationWebPage is available.*

The following properties **affect** the provider's **category**:

Criterion (Key in JSON File) | Description / Data Type / Unit of Measurement | Category A | Category B
---|---|---|---
[inBandRegistration](https://xmpp.org/extensions/xep-0077.html#usecases-register) | registration via XMPP client / true or false | n | registrationWebPage available
registrationWebPage | mappings from lower-case [two-letter (639-1) language code](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes) to link of web page language version or {} for n/a | inBandRegistration available | inBandRegistration available
rating[XmppComplianceTester](https://compliance.conversations.im) | pair of score (in percentage) and link to result | >= 90 | >= 90
rating[ImObservatory](https://xmpp.net) | pair of score (upper-case letter) and link to result | >= A | >= A
maximum[HttpFileUpload](https://xmpp.org/extensions/xep-0363.html)FileSize | -1 for n/a, 0 for no limit or number in megabytes (MB) | >= 20 | >= 0
maximum[HTTPFileUpload](https://xmpp.org/extensions/xep-0363.html)TotalSize | -1 for n/a, 0 for no limit or number in megabytes (MB)| >= 100 | >= 0
maximum[MessageArchiveManagement](https://xmpp.org/extensions/xep-0313.html)StorageTime | -1 for n/a, 0 for no limit or number in days | >= 7 | >= 0
professionalHosting | hosted in data center with regular backups, link to kind of hosting, requested statement of provider or "" for nonprofessional hosting | n | y
freeOfCharge | link to payment information or "" for free service | n | y
legalNotice | mappings from lower-case [two-letter (639-1) language code](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes) to link of legalNotice language version or {} for n/a | n | y
serverLocations | list of lower-case [two-letter country codes](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2) or [] for n/a | n | y
groupChatSupport | list of group chat addressses or [] for n/a | at least one other support available | y
adminChatSupport | list of admin chat addresses or [] for n/a | at least one other support available | y
emailSupport | list of email addresses or [] for n/a | at least one other support available | y
onlineSince | [ISO YYYY-MM-DD date](https://en.wikipedia.org/wiki/ISO_8601#Dates) or "" for n/a | older than 365 days | y

## Filter Script

The script *filter.py* can be used to create a filtered list of providers.

### Usage

You can see all options before you start running the script:
```
./filter.py --help
```

If you want to create a filtered list which can be used by a client, simply enter the category name.
For example, the following command creates a list of category *A* providers:
```
./filter.py -A
```
